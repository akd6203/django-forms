from django import forms
from django.core import validators
class myform(forms.Form):
    #CUSTOM VALIDATORS
    def name(value):
        if value[0] != 'a':
            raise forms.ValidationError("Name Must Start With a")
    def titlename(value):
        if not value.istitle():
            raise forms.ValidationError("Name Must be in Title case")
    name = forms.CharField(label="Enter Name", validators=[validators.MaxLengthValidator(5),name,titlename])
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Student Name..'}))
