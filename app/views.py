from django.shortcuts import render
from app.forms import myform

def index(request):
    fname = myform()
    if request.method == 'POST':
        fname= myform(request.POST)
        if fname.is_valid():
            name = fname.cleaned_data['name']
            e = fname.cleaned_data['email']
            return render(request ,'form.html',{'n':name,'e':e} )
    return render(request ,'form.html',{'f':fname} )
